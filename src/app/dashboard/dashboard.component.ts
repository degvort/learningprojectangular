import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router'
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms'

import { LocaleService, TranslationService, Language } from 'angular-l10n';

import { Article, ArticleToCreate, User, PageModel } from '../_models';
import { ArticleService } from '../_services'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  @Language() lang = '';
  error: "";
  news: Article[] = [];
  articleCreateForm: FormGroup;
  submitted = false;
  public Create: boolean = false;
  public isCreate: boolean;
  public author: string;
  public title: string;
  public description: string;
  public article: ArticleToCreate
  public response?: {'Result' : string}; 
  public currentUser : User = JSON.parse(localStorage.getItem("currentUser"));
  public pageModel : PageModel;
  

  constructor(private articleService: ArticleService,
              private fb: FormBuilder,
              public locale: LocaleService,
              public translation: TranslationService,
              private router: Router) { 
                this.articleCreateForm = fb.group(
                  {
                    Title: ['', [Validators.required]],
                    Description: ['', [Validators.required]],
                  });
              }

  ngOnInit() {
    this.articleService.getArticle(1).pipe(first()).subscribe((news: any) => {
      this.news = news.Result.Data;
      this.pageModel = news.Result.PageModel;
    });
    this.response = {'Result' : "//StaticFiles//Images//profile.jpg"};
  }

  get f() { return this.articleCreateForm.controls; }

 onSubmit = () => {
  this.submitted = true;
  if(this.articleCreateForm.invalid){
  return;
  };
    this.article = {
      Author: this.currentUser.Username,
      Title: this.f.Title.value,
      Description: this.f.Description.value,
      ImgPath: this.response.Result
    }
    this.articleService.createNew(this.article).pipe(first())
    .subscribe(data => {
      this.Create = false;
    },
    error => {
      this.error = error;
    });
    this.ngOnInit();
    location.reload();
  }


  public takePage(move: number){
    this.pageModel.PageNumber = this.pageModel.PageNumber + move;
    this.articleService.getArticle(this.pageModel.PageNumber).pipe(first()).subscribe((resp: any) => {
      this.news = resp.Result.Data;
      this.pageModel = resp.Result.PageModel;
    },
    error =>{
      this.error = error;
    });
  }
  public returnToCreate = () => {
    this.isCreate = true;
  }
  public uploadFinished = (event) => {
    this.response = event;
  }
}
