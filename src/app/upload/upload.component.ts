import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HttpEventType, HttpClient, HttpHeaders } from '@angular/common/http';
import { Language } from 'angular-l10n';
import { LocaleService } from 'angular-l10n';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {
  private headers: HttpHeaders;
  @Language() lang = '';
  error: "";
  public progress: number;
  public message: string;
  @Output() public onUploadFinished = new EventEmitter();

  constructor(private http: HttpClient,
    public locale: LocaleService,) { }

  ngOnInit() {
  }

  public uploadFile = (files) => {
    if (files.length === 0) {
      return;
    }
    this.headers = new HttpHeaders();
    this.headers = this.headers.set('Accept-Language', this.locale.getDefaultLocale()); 

    let fileToUpload = <File>files[0];
    const formData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);

    this.http.post(`http://localhost:51806/upload`, formData, {reportProgress: true, observe: 'events', headers: this.headers})
      .subscribe((event: any) => {
        if (event.type === HttpEventType.UploadProgress)
          this.progress = Math.round(100 * event.loaded / event.total);
        else if (event.type === HttpEventType.Response) {
          this.message = 'Upload success.';
          this.onUploadFinished.emit(event.body);
        }
      },error =>{
        this.error = error;
      } );
  }

}
