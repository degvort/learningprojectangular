import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { LocaleService } from 'angular-l10n';

import { User } from '../_models';

@Injectable({ providedIn: 'root' })
export class UserService {
  private headers: HttpHeaders;
  constructor(public locale: LocaleService, private http: HttpClient) { }
  
  getUsers(page ?: number){
    this.headers = new HttpHeaders();
    this.headers = this.headers.set('Accept-Language', this.locale.getDefaultLocale()); 
    return this.http.get<User[]>(`http://localhost:51806/users?page=` + page, {headers: this.headers});
  }
  register(username: string, password: string, confirmPassword: string){
    this.headers = new HttpHeaders();
    this.headers = this.headers.set('Accept-Language', this.locale.getDefaultLocale()); 
    return this.http.post(`http://localhost:51806/register`, {username, password, confirmPassword}, {headers: this.headers});
  }
  modify(user: User){
    this.headers = new HttpHeaders();
    this.headers = this.headers.set('Accept-Language', this.locale.getDefaultLocale()); 
    return this.http.post(`http://localhost:51806/modify`, user, {headers: this.headers} );
  }
}
