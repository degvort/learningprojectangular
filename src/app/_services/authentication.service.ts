import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../_models'
import { LocaleService } from 'angular-l10n';

@Injectable({ providedIn: 'root' })

export class AuthenticationService {
  private headers: HttpHeaders;
  private currrentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(public locale: LocaleService, private htpp: HttpClient) { 
    this.currrentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currrentUserSubject.asObservable();   
  }

  public get currentUserValue(): User{
    return this.currrentUserSubject.value;
  }

  login(username: string, password: string) {

    this.headers = new HttpHeaders();
    this.headers = this.headers.set('Accept-Language', this.locale.getDefaultLocale());    
    return this.htpp.post<any>(`http://localhost:51806/login`, { username, password }, {headers: this.headers})
      .pipe(map(res => {
        if (res.Result && res.Result.Token) {
          localStorage.setItem('currentUser', JSON.stringify(res.Result));
          this.currrentUserSubject.next(res.Result);
        }
        return res.Result;
      }));
  }

  logout() {
    localStorage.removeItem('currentUser');
    this.currrentUserSubject.next(null);
  }
}
