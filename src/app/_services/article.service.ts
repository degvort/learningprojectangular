import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { LocaleService } from 'angular-l10n';

import { Article, ArticleToCreate } from '../_models';

@Injectable({ providedIn: 'root' })
export class ArticleService {
  private headers: HttpHeaders;
  constructor(public locale: LocaleService, private http: HttpClient) {}

  getArticle(page? : number) {
    this.headers = new HttpHeaders();
    this.headers = this.headers.set('Accept-Language', this.locale.getDefaultLocale()); 
    return this.http.get<Article[]>('http://localhost:51806/api/Dashboard?page=' + page, {headers: this.headers});
  }
  createNew(article: ArticleToCreate){
    this.headers = new HttpHeaders();
    this.headers = this.headers.set('Accept-Language', this.locale.getDefaultLocale()); 
    return this.http.post('http://localhost:51806/createArticle', article, {headers: this.headers});
  }
}
