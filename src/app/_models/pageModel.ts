export class PageModel{
    PageNumber : number;
    TotalPages: number;
    HasPreviousPage: boolean;
    HasNextPage: boolean;
}