export class Article {
  id: number;
  author: string;
  title: string;
  description: string;
  imgPath: string;
  createTime: Date;
}
