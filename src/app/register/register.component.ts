import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms'
import { first } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';

import { LocaleService, TranslationService, Language } from 'angular-l10n';

import { UserService, AuthenticationService } from '../_services';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  @Language() lang = '';
  registerForm: FormGroup;
  loading = false;
  submitted = false;
  error: "";

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private fb: FormBuilder,
    public locale: LocaleService,
    public translation: TranslationService,
    public cookieService: CookieService,
  ) {
    this.registerForm = fb.group(
      {
        username: ['', [Validators.required, Validators.pattern('^[A-Za-z0-9]+$')]],
        password: ['', Validators.required],
        confirmPass: ['', [Validators.required]]
      }, { validator: this.userPasswordValidator});

    if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/']);
    }
    this.locale.defaultLocaleChanged.subscribe((item: string) => { this.onLanguageCodeChangedDataRecieved(item); });
  }

  ngOnInit() {
  }
  
  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    this.loading = true;
    this.userService.register(this.f.username.value, this.f.password.value, this.f.confirmPass.value)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(['/login']);
        },
        error => {
          this.error = error;
          this.loading = false;
        });
  }
  userPasswordValidator(AC: AbstractControl) {
    let password = AC.get('password').value;
    let confirmPassword = AC.get('confirmPass').value;
    return password === confirmPassword ? null : AC.get('confirmPass').setErrors({ MatchPassword: true })
  }
  public ChangeCulture(language: string, country: string) {
    this.locale.setDefaultLocale(language, country);
    this.cookieService.set('.AspNetCore.Culture', `c=${language}-${country}`);
  }
  private onLanguageCodeChangedDataRecieved(item: string) {
    console.log('onLanguageCodeChangedDataRecieved App');
    console.log(item);
  }
}
