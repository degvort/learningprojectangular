 import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatDialogModule } from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { Configuration } from './app.constants';
import { CookieService } from 'ngx-cookie-service';

import { routing } from './app.routing';

import { JwtInterceptor } from './_helpers/jwt.interceptor';
import { ErrorInterceptor } from './_helpers/error.interceptor';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RegisterComponent } from './register/register.component';
import { UsersComponent } from './users/users.component';

import { L10nConfig, L10nLoader, TranslationModule, StorageStrategy, ProviderType} from 'angular-l10n';
import { UploadComponent } from './upload/upload.component';

const l10nConfig: L10nConfig = {
  locale: {
      languages: [
          { code: 'en', dir: 'ltr' },
          { code: 'uk', dir: 'ltr' }
      ],
      language: 'en',
      storage: StorageStrategy.Cookie
  },
  translation: {
      providers: [
          { type: ProviderType.Static, prefix: '../assets/i18n/locale-' }
      ],
      caching: true,
      missingValue: 'No key'
  }
};

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    DashboardComponent,
    RegisterComponent,
    UsersComponent,
    UploadComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    routing,
    MatDialogModule,
    BrowserAnimationsModule,
    TranslationModule.forRoot(l10nConfig),
    CommonModule
  ],
  providers: [
    CookieService,
    Configuration,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent],
  exports: [LoginComponent, HomeComponent, DashboardComponent, RegisterComponent, UsersComponent, UploadComponent]
})
export class AppModule {
  constructor(public l10nLoader: L10nLoader){
    this.l10nLoader.load();
  }
 }
