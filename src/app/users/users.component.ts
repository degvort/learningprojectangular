import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router'

import { LocaleService, TranslationService, Language } from 'angular-l10n';

import { User } from '../_models';
import { UserService } from '../_services'
import { PageModel } from '../_models/pageModel';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  @Language() lang = '';
  pageModel : PageModel;
  users: User[] = [];
  error = '';
  selectedUser: User;

  constructor(
    private userService: UserService,
    public locale: LocaleService,
    public translation: TranslationService) { 
    }

  ngOnInit() {
    this.userService.getUsers(1).pipe(first()).subscribe((resp: any) => {
    this.users = resp.Result.Data;
    this.pageModel = resp.Result.PageModel;
    },
    error =>{
      this.error = error;
    });
  }

  takePage(move: number){
    this.pageModel.PageNumber = this.pageModel.PageNumber + move;
    this.userService.getUsers(this.pageModel.PageNumber).pipe(first()).subscribe((resp: any) => {
      this.users = resp.Result.Data;
      this.pageModel = resp.Result.PageModel;
    },
    error =>{
      this.error = error;
    });
  }

  onSelect(user: User): void{
    this.selectedUser = user;
  }
  onSubmit(){
    this.userService.modify(this.selectedUser)
      .pipe(first())
      .subscribe(
        data=>{
          this.selectedUser = null;
        }, 
        error => {
          this.error = error;
        });       
  }
}