import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from "rxjs/operators";
import { CookieService } from 'ngx-cookie-service';

import { LocaleService, TranslationService, Language } from 'angular-l10n';
import { AuthenticationService } from '../_services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @Language() lang = '';
  cookie: "";
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error: "";

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    public locale: LocaleService,
    public translation: TranslationService,
    public cookieService: CookieService) {
      this.locale.defaultLocaleChanged.subscribe((item: string) => { this.onLanguageCodeChangedDataRecieved(item); });
    }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.authenticationService.logout();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;
    if(this.loginForm.invalid){
    return;
    };
    
    this.authenticationService.login(this.f.username.value, this.f.password.value)
      .pipe(first())
      .subscribe(
      data => {
        this.router.navigate([this.returnUrl]);
        },
      error => {
          this.error = error;          
          this.loading = false;
        });
      }
    public ChangeCulture(language: string, country: string) {
        this.locale.setDefaultLocale(language, country);
        this.cookieService.set('.AspNetCore.Culture', `c=${language}-${country}`);
    }
    private onLanguageCodeChangedDataRecieved(item: string) {
        console.log('onLanguageCodeChangedDataRecieved App');
        console.log(item);
    }
}
