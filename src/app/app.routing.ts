import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { DashboardComponent } from './dashboard';
import { AuthGuard } from './_guards';
import { RegisterComponent } from './register';
import { UsersComponent } from './users';

const appRoutes: Routes = [
  { path: ' ', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'users', component: UsersComponent },
  { path: '**', redirectTo: ' '}
];

export const routing = RouterModule.forRoot(appRoutes);
