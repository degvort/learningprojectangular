import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LocaleService, TranslationService, Language } from 'angular-l10n';
import { CookieService } from 'ngx-cookie-service';

import { AuthenticationService } from './_services';
import { User } from './_models';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    @Language() lang = '';
    currentUser: User;

  constructor(
      public locale: LocaleService,
      public translation: TranslationService,
      private router: Router,
      private authenticationService: AuthenticationService,
      public cookieService: CookieService,
  ) {
      this.locale.defaultLocaleChanged.subscribe((item: string) => { this.onLanguageCodeChangedDataRecieved(item); });
      this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  logout() {
      this.authenticationService.logout();
      this.router.navigate(['/login']);
  }
  public ChangeCulture(language: string, country: string) {
    this.locale.setDefaultLocale(language, country); 
    this.cookieService.set('.AspNetCore.Culture', `c=${language}-${country}`);
}
private onLanguageCodeChangedDataRecieved(item: string) {
    console.log('onLanguageCodeChangedDataRecieved App');
    console.log(item);
}
}
